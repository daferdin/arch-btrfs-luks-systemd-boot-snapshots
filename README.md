# Arch Btrfs Luks Systemd Boot Snapshots

## Verificar si hay conexión
ip link (la conexión enp#s# debe indicar UP)<br />

## Si se desea hacer la instalación por SSH
pacman -Sy openssh <br />
systemctl enable --now sshd <br />
passwd <br />
ssh root@ip (check with **ip address**) <br />

## Crear particiones

gdisk /dev/nvme0n1 <br / >
o (Create a new empty GUID partition table (GPT)) <br />
Proceed? Y <br / >
n (Add a new partition) <br />
Partition number 1 <br / 
First sector (default) <br /> 
Last sector +512M <br />
Hex code EF00 <br />
n (Add a new partition) <br />
Partition number 2 <br />
First sector (default) <br />
Last sector (press Enter to use remaining disk) <br />
Hex code 8300 <br />
w <br />
Y <br />


